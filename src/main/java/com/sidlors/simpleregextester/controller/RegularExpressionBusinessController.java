package com.sidlors.simpleregextester.controller;

import java.awt.Color;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultHighlighter;
import javax.swing.text.Highlighter;

import com.sidlors.simpleregextester.model.RegexGroup;

public class RegularExpressionBusinessController {

    private RegularExpressionBusinessController(){}
	
   public static void testRegex(JTextPane inputPane, JTextField rxField, JTextPane outputPane) throws BadLocationException {
        String text = inputPane.getText();
        
        // This fixes possible issues with EOL characters by replacing them with Unix-style EOLs.
        text = text.replaceAll("\r\n", "\n");
        inputPane.setText(text);
        
        String pattern = rxField.getText();
        if (pattern.length() == 0) { return; }
        
        Highlighter hltr = inputPane.getHighlighter();
        Highlighter.Highlight[] highlights = hltr.getHighlights();
        
        for (Highlighter.Highlight h : highlights) {
            if (h.getPainter() instanceof DefaultHighlighter.DefaultHighlightPainter) {
                hltr.removeHighlight(h);
            }
        }
        
        outputPane.setText("");
        
        if (text.length() > 0) {
            List<List> highlightPositions = new ArrayList<>();
            int count = 0;
            
            StringBuilder sb = new StringBuilder();
            
            ArrayList<String> regexGroups = getRegexGroups(pattern);
          
            try {
                Pattern r = Pattern.compile(pattern);
                Matcher m = r.matcher(text);
                while (m.find()) {
                    count++;
                    
                    ArrayList<Integer> currentHighlightPositions = new ArrayList<>();
                    currentHighlightPositions.add(m.start());
                    currentHighlightPositions.add(m.end());
                    highlightPositions.add(currentHighlightPositions);
                    
                    sb.append(String.format("Match %d\n", count));
                    for (int i = 0; i <= m.groupCount(); i++) {
                        if (m.start(i) >= 0 && m.end(i) >= 0) {
                            String matchPosString = String.format("pos. %d", m.start(i));
                            matchPosString += String.format(", length %d", m.end(i) - m.start(i));
                            sb.append(String.format("   Group%d (%s): %s   =>   '%s'\n", i, matchPosString, regexGroups.get(i), m.group(i)));
                        }
                    }
                    sb.append("- - - - -\n");
                }
            }
            catch (Exception e) {
                outputPane.setForeground(Color.red);
                outputPane.setText(e.getMessage());
                return;
            }
           
            if (count > 0) {
                for (List curPositions : highlightPositions) {
                    hltr.addHighlight(Integer.parseInt(curPositions.get(0).toString()),
                            Integer.parseInt(curPositions.get(1).toString()),
                            new DefaultHighlighter.DefaultHighlightPainter(Color.green));
                }
                
                outputPane.setForeground(Color.blue);
                outputPane.setText(sb.toString());
            }
            else {
                outputPane.setForeground(Color.red);
                outputPane.setText("NO MATCH!");
            }
        }
    }

   private static ArrayList<String> getRegexGroups(String rx) {
        ArrayList<String> rxGroups = new ArrayList<>();
        Deque<Integer> openingBracketPositions = new ArrayDeque<>();
        List<Integer> closingBracketPositions = new ArrayList<>();
        List<RegexGroup> matchingGroups = new ArrayList<>();
        
        rxGroups.add(rx); 

        char[] rxChars = rx.toCharArray();
        
        for (int i = 0; i < rxChars.length; i++) {
            String stringBefore = new String(rxChars, 0, i);
            char curChar = rxChars[i];
            if (curChar == '(' && !hasEscapeSequence(stringBefore)) {
                openingBracketPositions.push(i);
            }
            else if (curChar == ')' && !hasEscapeSequence(stringBefore)) {
                closingBracketPositions.add(i);
            }
        }

        while (openingBracketPositions.size() > 0) {
            Integer[] currentGroup = new Integer[2];
            currentGroup[0] = openingBracketPositions.pop();

            for (int i = 0; i < closingBracketPositions.size(); i++) {
                if (closingBracketPositions.get(i) > currentGroup[0]) {
                    currentGroup[1] = closingBracketPositions.get(i);
                    closingBracketPositions.remove(i);
                    break;
                }
            }

            RegexGroup curRegexGroup = new RegexGroup(currentGroup[0], currentGroup[1], rx);
            matchingGroups.add(curRegexGroup);
        }

        matchingGroups.sort(RegexGroup::compareMC);

        matchingGroups.forEach((group) -> rxGroups.add(group.toString()));

        return rxGroups;
    }
    
   private static boolean hasEscapeSequence(String s) {
        int backslashCount = 0;
        if (s.length() > 0) {
            char[] stringChars = s.toCharArray();
            
            for (int i = stringChars.length - 1; i >= 0; --i) {
                if (stringChars[i] == '\\') {
                    ++backslashCount;
                }
                else {
                    break;
                }
            }
        }

        return backslashCount % 2 == 1;
    }

}
